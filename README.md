# _Zeszyty Komiksowe_ - Web Site Rewrite #

### About _Zeszyty Komiksowe_ ###

[_Zeszyty Komiksowe_](www.zeszytykomiksowe.org) is the Polish magazine of comics critique and study, published twice yearly since 2004.  It is entirely a non-profit operation (nearly all people involved are volunteers).

### The Rewrite ###

_Zeszyty_'s web site has been written several years ago in plain PHP.  Recently we have obtained a grant to rewrite a part of our web site - and we've decided to take this opportunity and rewrite the entire web site in a more CMS style.

We are using Laravel in this project.

### Project Status ###

The rewrite is still very much work in progress.  There is no point in cloning or forking this repo at this stage - it won't do you any good, and it isn't even a good example of the use of Laravel of web programming given that I am using this project as a way to improve my skills  :)

### Who do I talk to? ###

The person responsible for this repo is Michal Blazejczyk, the editor in chief of _Zeszyty Komiksowe_.  I have 15 years of experience in the software development industry, 13 as a developer and the last two as a manager.

### License ###

This code is provided using the [MIT license](http://opensource.org/licenses/MIT).