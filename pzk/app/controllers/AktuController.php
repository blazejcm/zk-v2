<?php

class AktuController extends \BaseController
{

  /**
   * Show the form for creating a new resource.
   *
   * @return Response
   */
  public function create()
  {
    return View::make( 'aktualnosci.edit' );
  }

  /**
   * Store a newly created resource in storage.
   *
   * @return Response
   */
  public function store()
  {
    $input = Input::all();
    $validation = Validator::make( $input, Aktualnosc::$rules );

    if( $validation->passes() )
    {
      Aktualnosc::create( $input );
      return Redirect::route('glowna');
    }
    else
    {
      return Redirect::route('aktualnosc.create')
        ->withInput()
        ->withErrors( $validation )
        ->with( 'message', 'Formularz zawiera błędy.' );
    }
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function show( $id )
  {
    return "Pokaż aktualność " . $id;
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function edit( $id )
  {
    $aktualnosc = Aktualnosc::find( $id );
    if( is_null( $aktualnosc ) )
    {
      return Redirect::route('glowna')
        ->with( 'problem', 'Aktualność ' . $id . ' nie istnieje.' );
    }
    else
    {
      return View::make( 'aktualnosci.edit', compact( 'aktualnosc' ) );
    }
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function update( $id )
  {
    $input = Input::all();
    $validation = Validator::make( $input, Aktualnosc::$rules );
    if( $validation->passes() )
    {
        $aktu = Aktualnosc::find( $id );
        $aktu->update( $input );
        return Redirect::route( 'glowna' );
    }
    else
    {
      return Redirect::route( 'aktualnosc.edit', $id )
        ->withInput()
        ->withErrors( $validation )
        ->with( 'message', 'Formularz zawiera błędy.' );
    }
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function destroy($id)
  {
    Aktualnosc::find( $id )->delete();
    return Redirect::route( 'glowna' );
  }

}