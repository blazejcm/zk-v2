<?php

class KopalniaController extends \BaseController
{

  /**
   * Display the index page.
   *
   * @return Response
   */
  public function index()
  {
    return View::make( 'kopalnia.index' );
  }

  /**
   * Display the specified resource.
   *
   * @param  int $id
   *
   * @return Response
   */
  public function show( $id )
  {
    $kopalnia = Kopalnia::find( $id );
    if( is_null( $kopalnia ) )
    {
      return Redirect::route( 'kopalnia.index' )
        ->with( 'problem', 'Fiszka z identyfikatorem ' . $id . ' nie istnieje.' );
    } else
    {
      return View::make( 'kopalnia.view', compact( 'kopalnia' ) );
    }
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return Response
   */
  public function create()
  {
    return View::make( 'kopalnia.index' );
  }

  /**
   * Store a newly created resource in storage.
   *
   * @return Response
   */
  public function store()
  {
    return Redirect::route( 'kopalnia.index' );
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int $id
   *
   * @return Response
   */
  public function edit( $id )
  {
    return Redirect::route( 'kopalnia.index' );
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  int $id
   *
   * @return Response
   */
  public function update( $id )
  {
    return Redirect::route( 'kopalnia.index' );
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int $id
   *
   * @return Response
   */
  public function destroy( $id )
  {
    return Redirect::route( 'kopalnia.index' );
  }

  /**
   * Remove the specified resource from storage.
   *
   * @return Response
   */
  public function search()
  {
    return "Search page!";
  }

}