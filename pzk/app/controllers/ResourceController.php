<?php

class ResourceController extends \BaseController
{

  /**
   * Return all images from the given path as JSON.
   *
   * @return JSON
   */
  public function getObrazki( $path )
  {
    $data = array();
    $full_path = public_path() . '/img/' . $path;

    if( $h = opendir( $full_path ) )
    {
      $files = array();
      while( ( $filename = readdir( $h ) ) !== false )
      {
        if( $filename != "." && $filename != ".." )
        {
          $files[ $filename ] = filemtime( $full_path . "\\" . $filename );
        }
      }

      closedir( $h );

      arsort( $files );

      foreach( $files as $filename => $ctime )
      {
        array_push( $data, array(
          'created' => $ctime,
          'path' => $filename ) );
      }
    }

    return Response::json( $data );
  }

}