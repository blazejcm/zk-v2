<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAktualnosciTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create( 'aktualnosci', function( Blueprint $table )
		{
			$table->increments( 'id' );
			$table->timestamps();
			$table->string( 'Data', 25 );
			$table->string( 'Nazwa', 30 );
			$table->string( 'Obrazek', 64 )->nullable();
			$table->string( 'ObrazekUrl', 512 )->nullable();
			$table->text( 'Tresc' );			
		} );
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop( 'aktualnosci' );
	}

}
