<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKopalniaTables extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
    Schema::create( 'uzytkownicy', function ( Blueprint $table )
    {
      $table->increments( 'id' );
      $table->timestamps();
      $table->string( 'username', 50 );
      $table->string( 'password', 64 );
      $table->string( 'Imiona', 50 );
      $table->string( 'Nazwisko', 50 );
      $table->string( 'Email', 120 );
    } );

    Schema::create( 'autorzy', function ( Blueprint $table )
    {
      $table->increments( 'id' );
      $table->timestamps();
      $table->string( 'Imiona', 50 )->nullable();
      $table->string( 'Nazwisko', 50 );
    } );

    Schema::create( 'wydawcy', function ( Blueprint $table )
    {
      $table->increments( 'id' );
      $table->timestamps();
      $table->string( 'Nazwa', 50 );
    } );

    Schema::create( 'hasla_przedm', function ( Blueprint $table )
    {
      $table->increments( 'id' );
      $table->timestamps();
      $table->string( 'Haslo', 50 );
    } );

    Schema::create( 'kopalnia', function ( Blueprint $table )
    {
      $table->increments( 'id' );
      $table->timestamps();

      $table->unsignedInteger( 'RodzicId' )->nullable()->index();
      $table->foreign( 'RodzicId' )->references( 'id' )->on( 'kopalnia' );
      $table->unsignedInteger( 'DzialId' )->nullable()->index();
      $table->foreign( 'DzialId' )->references( 'id' )->on( 'kopalnia' );
      $table->unsignedInteger( 'KtoDopisalId' )->nullable();
      $table->foreign( 'KtoDopisalId' )->references( 'id' )->on( 'uzytkownicy' );
      // `autorzy`, `wydawcy` and `hasla_przedm` are linked many-to many.

      $table->string( 'Tytul', 175 );
      $table->string( 'Rodzic', 200 )->nullable();
      $table->string( 'Isbn', 15 )->nullable();
      $table->string( 'MiejsceWydania', 40 )->nullable();
      $table->string( 'Objetosc', 30 )->nullable();
      $table->char( 'Jezyk', 2 )->nullable()->default( 'pl' );
      $table->enum( 'Rodzaj', array( 'pismo', 'kolekcja', 'numer', 'portal', 'dzial', 'ksiazka', 'praca', 'artykul', 'rozdzial', 'wywiad', 'audiowideo', 'audycja', 'notka', 'inny' ) )->default( 'artykul' );

      $table->tinyInteger( 'PubMiesiac' )->unsigned()->nullable();
      $table->mediumInteger( 'PubRok' )->unsigned()->nullable();
      $table->tinyInteger( 'CzyBrakujeInfo' )->unsigned()->default( 0 );

      $table->text( 'Nota' )->nullable();
      $table->text( 'Opis' )->nullable();
      $table->text( 'SlowaKluczowe' )->nullable();

      // Do we still need a fulltext index???  Probably not...
      // FULLTEXT KEY `PelenTekst` ( `Tytul`,`Autorzy`,`Nota`)
      // DB::statement( 'ALTER TABLE kopalnia ADD FULLTEXT PelenTekst (Tytul,Autorzy,Nota)' );
    } );

    Schema::create( 'kopalnia_autorzy', function ( Blueprint $table )
    {
      $table->increments( 'id' );
      $table->unsignedInteger( 'KopalniaId' )->index();
      $table->foreign( 'KopalniaId' )->references( 'id' )->on( 'kopalnia' );
      $table->unsignedInteger( 'AutorId' )->index();
      $table->foreign( 'AutorId' )->references( 'id' )->on( 'autorzy' );
      $table->enum( 'Typ', array( 'autor', 'redaktor', 'tlumacz', 'wywiadowca' ) )->default( 'autor' );
    } );

    Schema::create( 'kopalnia_wydawcy', function ( Blueprint $table )
    {
      $table->increments( 'id' );
      $table->unsignedInteger( 'KopalniaId' )->index();
      $table->foreign( 'KopalniaId' )->references( 'id' )->on( 'kopalnia' );
      $table->unsignedInteger( 'WydawcaId' )->index();
      $table->foreign( 'WydawcaId' )->references( 'id' )->on( 'wydawcy' );
    } );

    Schema::create( 'kopalnia_hasla_przedm', function ( Blueprint $table )
    {
      $table->increments( 'id' );
      $table->unsignedInteger( 'KopalniaId' )->index();
      $table->foreign( 'KopalniaId' )->references( 'id' )->on( 'kopalnia' );
      $table->unsignedInteger( 'HasloId' )->index();
      $table->foreign( 'HasloId' )->references( 'id' )->on( 'hasla_przedm' );
    } );
  }

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
    Schema::drop( 'kopalnia_autorzy' );
    Schema::drop( 'kopalnia_wydawcy' );
    Schema::drop( 'kopalnia_hasla_przedm' );
//    Schema::table( 'kopalnia', function ( $table )
//    {
//      $table->dropIndex( 'PelenTekst' );
//    } );
    Schema::drop( 'kopalnia' );
    Schema::drop( 'autorzy' );
    Schema::drop( 'wydawcy' );
    Schema::drop( 'hasla_przedm' );
    Schema::drop( 'uzytkownicy' );
  }

}
