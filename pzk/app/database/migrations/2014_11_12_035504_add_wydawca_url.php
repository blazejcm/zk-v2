<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddWydawcaUrl extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
    Schema::table( 'wydawcy', function ( Blueprint $table )
    {
      $table->string( 'Url', 250 )->nullable();
    } );
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
    Schema::table( 'wydawcy', function ( Blueprint $table )
    {
      $table->dropColumn( 'Url' );
    } );
  }

}
