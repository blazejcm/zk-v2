<?php

class ZkHtml {

  // This function is not really needed because I found a way to achieve the same result using an <a>.
  public static function button( $text, $controller )
  {
    return Form::open( array(
      'action' => $controller,
      'method' => 'get',
      'style' => 'display: inline' ) )
      . Form::submit( $text, array( 'class' => 'btn btn-xs btn-zk' ) )
      . Form::close();
  }

  public static function errorIcon( $field )
  {
    $errors = Session::get( 'errors' );
    $error = $errors && $errors->has( $field ) ? $errors->first( $field ) : '';
    $visible = $error == '' ? ' display: none;' : '';
    return "<span rel='tooltip' title='" . $error . "'"
      . " id='" . $field . "-error'"
      . " class='glyphicon glyphicon-exclamation-sign error'" 
      . " style='cursor: default;" . $visible . "'></span> ";
  }

  public static function label( $field, $text )
  {
    $errors = Session::get( 'errors' );
    $error = $errors && $errors->has( $field ) ? $errors->first( $field ) : '';
    $labelName = $field . "-label";
    $label = ( $error == ''
      ? Form::label( $field, $text, array( "id" => $labelName ) )
      : Form::label( $field, $text, array( "id" => $labelName, "class" => "error" ) ) );
    return self::errorIcon( $field) . $label;
  }

  public static function formErrorAlert()
  {
    $visible = ( Session::has( 'message' ) ? "" : " style='display: none;'" );
    $msg = ( Session::has( 'message' ) ? Session::get( 'message' ) : "" );
    return "<div class='alert alert-danger' id='form-alert'" . $visible . "><p>" . $msg . "</p></div>";
  }

}

?>