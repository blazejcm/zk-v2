<?php

//use Illuminate\Auth\UserInterface;
//use Illuminate\Auth\Reminders\RemindableInterface;

class Aktualnosc extends Eloquent
{
	protected $table = 'aktualnosci';

	protected $primaryKey = 'id';

	protected $guarded = array( 'id' );

	public static $rules = array(
		'Data' => 'required|min:5|max:25',
		'Nazwa' => 'required|min:3|max:30',
		'Tresc' => 'required|max:10000',
		'Obrazek' => 'max:64',
		'ObrazekUrl' => 'max:512'
	);

	public static function obrazekPath( $obrazek = null )
	{
		return public_path() . "/img/aktualnosci" . ( $obrazek != null ? '/' . $obrazek : '' );
	}

	private static function processFormInputs( array $attributes = array() )
	{
    // UsunObrazek will only be present for 'update' requests, not for 'create'.
	  if( isset( $attributes[ 'UsunObrazek' ] ) )
    {
      if( $attributes[ 'UsunObrazek' ] == 'Usun' )
      {
        // If 'tab-select-hdr' is selected, we should let the user select an image -
        // so we shouldn't unset 'Obrazek'.
        if( $attributes[ 'ObrazekTab' ] != 'tab-select-hdr' )
        {
          // Setting this to null will actually set it to NULL in the database as well.
          $attributes[ 'Obrazek' ] = null;
        }
      }
	    unset( $attributes[ 'UsunObrazek' ] );
	  }

    // 'Obrazek' is only allowed when 'tab-select-hdr' is selected.
    if( $attributes[ 'Obrazek' ] != null && $attributes[ 'ObrazekTab' ] != 'tab-select-hdr' )
    {
      unset( $attributes[ 'Obrazek' ] );
    }

   	$file = $attributes[ 'ObrazekPlik' ];
  	if( $file != null && $attributes[ 'ObrazekTab' ] == 'tab-upload-hdr' /*id of the div representing the tab*/ )
  	{
      // In case client-side file name validation failed, replace all accented characters with _.
      $originalName = $file->getClientOriginalName();
      $finalName = preg_replace( '/[^\x{0020}-\x{007e}]/u', '_', $originalName );
  		// Set the 'Obrazek' database field.
			$attributes[ 'Obrazek' ] = $finalName;
  		// Move the new file to the right directory.
			$file->move( self::obrazekPath(), $finalName );
		}

		// Must remove from $attributes any fields that do not have a corresponding database field.
		unset( $attributes[ 'ObrazekPlik' ] );
    unset( $attributes[ 'ObrazekTab' ] );

    return $attributes;
	}

  public static function create( array $attributes = array() )
  {
  	return parent::create( self::processFormInputs( $attributes ) );
  }

  public static function createRaw( array $attributes = array() )
  {
    return parent::create( $attributes );
  }

	public function update( array $attributes = array() )
  {
  	return parent::update( self::processFormInputs( $attributes ) );
  }
}