<?php
/**
 * Created by PhpStorm.
 * User: Michal
 * Date: 21/06/14
 * Time: 9:32 PM
 */

use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableInterface;

class Attachment extends Eloquent
{

  /**
   * The database table used by the model.
   *
   * @var string
   */
  protected $table = 'attachments';

  protected $primaryKey = 'id';

  protected $guarded = array( 'id' );

  public static $rules = array(
    'RefId' => 'required|integer|min:1',
    'RefTable' => 'required|max:40',
    'Plik' => 'required|max:256'
  );

}