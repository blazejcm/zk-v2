<?php

class Autor extends Eloquent
{
  protected $table = 'autorzy';

  protected $primaryKey = 'id';

  protected $guarded = array( 'id' );

  public static $rules = array(
    'Imiona' => 'max:50',
    'Nazwisko' => 'required|max:50',
  );

  private $typy = array(
    'autor' => 2, 'redaktor' => 1, 'tlumacz' => 4, 'wywiadowca' => 3 );

  public function Typ()
  {
    return $this->pivot->Typ;
  }

  public function TypNum()
  {
    return $this->typy[ $this->pivot->Typ ];
  }
}