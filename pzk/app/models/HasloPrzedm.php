<?php

class HasloPrzedm extends Eloquent
{
  protected $table = 'hasla_przedm';

  protected $primaryKey = 'id';

  protected $guarded = array( 'id' );

  public static $rules = array(
    'Haslo' => 'required|max:50',
  );

} 