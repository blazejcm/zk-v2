<?php

class Kopalnia extends Eloquent
{
  protected $table = 'kopalnia';

  protected $primaryKey = 'id';

  protected $guarded = array( 'id' );

  public static $rules = array(
    'Rodzic' => 'max:200',
    'Isbn' => 'max:15',
    'Tytul' => 'required|max:175',
    'CzyBrakujeInfo' => 'integer|min:0|max:1',
    'PubMiesiac' => 'integer|min:1|max:12',
    'PubRok' => 'integer|min:1800|max:2050',
    'MiejsceWydania' => 'max:40',
    'Rodzaj' => 'required|in:pismo,kolekcja,numer,portal,dzial,ksiazka,praca,artykul,rozdzial,wywiad,audiowideo,audycja,notka,inny',
    'Objetosc' => 'max:30',
    'Jezyk' => 'max:2',
  );

  private $rodzaje = array(
    "pismo" => "czasopismo",
    "numer" => "numer",
    "portal" => "portal",
    "dzial" => "dział",
    "artykul" => "artykuł",
    "wywiad" => "wywiad",
    "notka" => "notka prasowa",
    "kolekcja" => "kolekcja",
    "ksiazka" => "książka",
    "rozdzial" => "rozdział",
    "praca" => "praca naukowa",
    "audycja" => "audycja",
    "audiowideo" => "nagranie audio lub wideo",
    "inny" => "inny" );

  private $jezyki = array(
    "pl" => "polski", "en" => "angielski", "fr" => "francuski", "ru" => "rosyjski",
    "de" => "niemiecki", "ua" => "ukraiński", "cz" => "czeski", "sk" => "słowacki",
    "lt" => "litewski", "se" => "szwedzki", "es" => "hiszpański", "it" => "włoski",
    "??" => "inny" );

  public function PrintRodzaj()
  {
    return $this->rodzaje[ $this->Rodzaj ];
  }

  public function PrintJezyk()
  {
    return $this->jezyki[ $this->Jezyk ];
  }

  public function zawartosc()
  {
    return $this->hasMany( 'Kopalnia', 'RodzicId' );
  }

  public function rodzic()
  {
    return $this->belongsTo( 'Kopalnia', 'RodzicId' );
  }

  public function zawartoscDzialu()
  {
    return $this->hasMany( 'Kopalnia', 'DzialId' );
  }

  public function dzial()
  {
    return $this->belongsTo( 'Kopalnia', 'DzialId' );
  }

  public function autorzy()
  {
    // To get to the Typ field, call $author->pivot->Typ
    return $this->belongsToMany( 'Autor', 'kopalnia_autorzy', 'KopalniaId', 'AutorId' )->withPivot( array( 'Typ' ) );
  }

  public function tylkoAutorzy( $typ )
  {
    return $this->autorzy->filter( function ( $a ) use ( $typ )
    {
      return $a->Typ() == $typ;
    } );
  }

  public function autorzyView()
  {
    $items = $this->autorzy->map( function ( $a )
    {
      $item = "<a href=\"#\">{$a->Imiona} {$a->Nazwisko}</a>";
      if( 'autor' == $a->Typ() )
      {
        $item = "<b>$item</b>";
      } else if( 'redaktor' == $a->Typ() )
      {
        $item .= " (redakcja)";
      } else if( 'tlumacz' == $a->Typ() )
      {
        $item .= " (tłumaczenie)";
      } else if( 'wywiadowca' == $a->Typ() )
      {
        $item .= " (zadawał/a pytania)";
      }

      return $item;
    } );

    return implode( ", ", $items->all() );
  }

  public function wydawcy()
  {
    return $this->belongsToMany( 'Wydawca', 'kopalnia_wydawcy', 'KopalniaId', 'WydawcaId' );
  }

  public function wydawcyView()
  {
    $items = $this->wydawcy->map( function ( $w )
    {
      $item = $w->Nazwa;
      if( $w->Url != null )
      {
        $item = "<a href=\"{$w->Url}\">$item</a>";
      }
      $item = "<b>$item</b>";
      return $item;
    } );

    return implode( ", ", $items->all() );
  }

  public function haslaPrzedmiotowe()
  {
    return $this->belongsToMany( 'HasloPrzedm', 'kopalnia_hasla_przedm', 'KopalniaId', 'HasloId' );
  }

  public function haslaPrzedmiotoweView()
  {
    $items = $this->haslaPrzedmiotowe->map( function ( $h )
    {
      $item = "<a href=\"#\">$h->Haslo</a>";
      return $item;
    } );

    return implode( "<br> ", $items->all() );
  }
}