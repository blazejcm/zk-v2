<?php

class Uzytkownik extends Eloquent
{
  protected $table = 'uzytkownicy';

  protected $primaryKey = 'id';

  protected $guarded = array( 'id', 'password' );

  public static $rules = array(
    'Imiona' => 'required|max:50',
    'Nazwisko' => 'required|max:50',
    'Email' => 'required|email|max:120',
    'username' => 'required|max:50'
  );

} 