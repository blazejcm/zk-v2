<?php

class Wydawca extends Eloquent
{
  protected $table = 'wydawcy';

  protected $primaryKey = 'id';

  protected $guarded = array( 'id' );

  public static $rules = array(
    'Nazwa' => 'required|max:50',
    'Url' => 'max:250',
  );

} 