<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get( '/', array( 'as' => 'glowna', function() {
	return View::make( 'glowna' );
} ) );

Route::resource( 'aktualnosc', 'AktuController',
  array( 'except' => array( 'index' ) ) );

Route::resource( 'kopalnia', 'KopalniaController' );
Route::get( 'kopalnia/search', array( 'as' => 'kopalnia.search', 'uses' => 'KopalniaController@search' ) );

Route::get( 'json/obrazki/{path}', 'ResourceController@getObrazki' );

Route::get('o_stronie', function()
{
  return View::make('o_stronie');
});

Route::get('contact', 'PagesController@contact');

Route::resource('users', 'UsersController');