<?php

// Running phpunit: ./vendor/bin/phpunit
class KopalniaTest extends TestCase
{

  public function testRelationships()
  {
    $zk = Kopalnia::find( 1 );
    $dzial = Kopalnia::find( 2 );
    $artykul1 = Kopalnia::find( 3 );
    $artykul2 = Kopalnia::find( 4 );

    $this->assertEquals( 1, count( $zk->autorzy ) );
    $this->assertEquals( 0, count( $dzial->autorzy ) );
    $this->assertEquals( 2, count( $artykul1->autorzy ) );
    $this->assertEquals( 4, count( $artykul2->autorzy ) );
    $this->assertEquals( 1, count( $artykul2->autorzy()->where( 'relations.Typ', '=', 'redaktor' ) ) );
    $this->assertEquals( 1, count( $artykul2->autorzy()->where( 'relations.Typ', '=', 'tlumacz' ) ) );
    $this->assertEquals( 2, count( $zk->wydawcy ) );
    $this->assertEquals( 0, count( $dzial->wydawcy ) );
    $this->assertEquals( $dzial->rodzic->id, $zk->id );
    $this->assertEquals( $artykul1->dzial->id, $dzial->id );
    $this->assertEquals( 2, count( $dzial->zawartoscDzialu ) );
    $this->assertEquals( 3, count( $zk->zawartosc ) );
  }

  public function testSort()
  {
    $art = Kopalnia::find( 4 );
    $data = array( 0 => $art->autorzy[ 0 ], 1 => $art->autorzy[ 1 ], 2 => $art->autorzy[ 2 ], 3 => $art->autorzy[ 3 ] );

    $sort = function ( $autor1, $autor2 )
    {
      $typ1 = $autor1->typNum();
      $typ2 = $autor2->typNum();
      $result = ( $typ1 == $typ2 ? 0 : ( $typ1 > $typ2 ? 1 : -1 ) );
      echo "Comparing " . $typ1 . " to " . $typ2 . " yields " . $result . "\n";
      return $result;
    };

    foreach( $data as $key => $aut )
    {
      echo $aut->Nazwisko . "\n";
    }

    uasort( $data, $sort );

    foreach( $data as $key => $aut )
    {
      echo $aut->Nazwisko . "\n";
    }
  }

  public function testAutorzy()
  {
    $artykul2 = Kopalnia::find( 4 );

    $autor = $artykul2->tylkoAutorzy( 'autor' );

    $this->assertEquals( 1, $autor->count() );
    $this->assertEquals( 'Gawronkiewicz', $autor[ 0 ]->Nazwisko );

//    $c = $artykul2->autorzy->map( function ( $a )
//    {
//      return $a->Imiona . " " . $a->Nazwisko;
//    } );
//    var_dump( implode( ", ", $c->all() ) );
  }
}