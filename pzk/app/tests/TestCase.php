<?php

class TestCase extends Illuminate\Foundation\Testing\TestCase {

	/**
	 * Creates the application.
	 *
	 * @return \Symfony\Component\HttpKernel\HttpKernelInterface
	 */
	public function createApplication()
	{
		$unitTesting = true;

		$testEnvironment = 'testing';

		return require __DIR__.'/../../bootstrap/start.php';
	}

  /**
   * Default preparation for each test.
   * See http://code.tutsplus.com/tutorials/testing-like-a-boss-in-laravel-models--net-30087
   */
  public function setUp()
  {
    parent::setUp(); // Don't forget this!

    Artisan::call( 'migrate' );
    Artisan::call( 'db:seed' );
    Mail::pretend( true );
  }

}
