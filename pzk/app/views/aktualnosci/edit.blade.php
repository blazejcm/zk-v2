@extends('layouts.main')

@section('main')

@include('partials.zk-gallery')

<div align="center">
  <table width="600" border="0" cellspacing="0" cellpadding="10">
    <tr>
      <td bgcolor="white" colspan="2" class="blackcourier">

        <h2 align="center" id="page-header">
<?php
  if( isset( $aktualnosc ) )
    echo "Modyfikuj aktualność";
  else
    echo "Nowa aktualność";
?>
        </h2>

        {{ ZkHtml::formErrorAlert() }}

        <p>

        </p>

<?php
  if( isset( $aktualnosc ) )
    echo Form::model( $aktualnosc, array(
      'method' => 'PATCH',
      'route' => array( 'aktualnosc.update', $aktualnosc->id ),
      'files' => true ) );
  else
    echo Form::open( array(
      'route' => 'aktualnosc.store',
      'files' => true ) );
?>

        {{ Form::hidden('ObrazekTab', null, array('id' => 'obrazek-tab-hidden')) }}

        <div class="panel panel-default">
          <div class="panel-heading strong">PODSTAWOWE INFORMACJE</div>
          <div class="panel-body">
            <p>
              {{ ZkHtml::label('Data', 'Data:') }}<br>
              {{ Form::text( 'Data', null, array( 'id' => 'Data',
                'data-validate' => 'true', 'data-min-length' => 5, 'data-max-length' => 25 ) ) }}
            </p>

            <p>
              {{ ZkHtml::label('Nazwa', 'Nazwa:') }}<br>
              {{ Form::text( 'Nazwa', null, array( 'id' => 'Nazwa',
                'data-validate' => 'true', 'data-min-length' => 3, 'data-max-length' => 30 ) ) }}
            </p>
          </div>
        </div>

@if( isset( $aktualnosc ) )
        <div class="panel panel-default">
          <div class="panel-heading strong">AKTUALNY OBRAZEK</div>
          <div class="panel-body">
@if( $aktualnosc->Obrazek )
            <p style="float: left;">
              <img src="{{ asset( 'img/aktualnosci/' . $aktualnosc->Obrazek ) }}" style="border-style: solid; border-width: 1px; border-style: dashed; border-color: darkgray;">
            </p>
            <p>
              <label style="font-weight: normal; padding-left: 15px;">
                {{ Form::checkbox( 'UsunObrazek', 'Usun' ) }}
                Usuń
              </label>
            </p>
@else
            <p>
              <b>Brak</b>
            </p>
@endif
          </div>
        </div>
@endif

        <div class="panel panel-default">
          <div class="panel-heading strong" id="Obrazki-label">
            OBRAZKI
          </div>
          <div class="panel-body">
            <ul class="nav nav-tabs" id="obrazki-tab">
              <li class="active" id="tab-upload-hdr"><a href="#tab-upload" data-toggle="tab">Nagraj nowy obrazek</a></li>
              <li id="tab-select-hdr"><a href="#tab-select" data-toggle="tab">Wybierz istniejący</a></li>
              <li id="tab-attachment-hdr"><a href="#tab-attachment" data-toggle="tab">Wybierz załącznik</a></li>
            </ul>
            <div class="tab-content" style="margin-top: 15px;">
              <div class="tab-pane fade in active" id="tab-upload">
                <p>
                  {{ ZkHtml::label( 'ObrazekPlik', 'Obrazek:' ) }}<br>
                  <span id="obrazek-upload-field-wrapper">
                    {{ Form::file( 'ObrazekPlik', array( 'id' => 'ObrazekPlik',
                         'data-validate' => 'true', 'data-ascii-only' => 'true' ) ) }}
                  </span>
                </p>
              </div>
              <div class="tab-pane fade" id="tab-select">
                <p>
                  <!-- Button to trigger the modal. -->
                  <a href="#" id="trigger-popup" class="btn btn-xs btn-zk" data-toggle="modal" data-target="#gallery-modal">
                    Wybierz obrazek ...
                  </a>
                </p>
                <p>
                  {{ ZkHtml::label('Obrazek', 'Obrazek:') }}<br>
                  {{ Form::text('Obrazek', null, array('style' => 'width: 50%;', 'id' => 'obrazek-field')) }}
                </p>
              </div>
              <div class="tab-pane fade" id="tab-attachment">
                <p>
                  ...
                </p>
              </div>
            </div>
            <p>
              {{ ZkHtml::label('ObrazekUrl', 'URL linku obrazka:') }}<br>
              {{ Form::text('ObrazekUrl', null, array('style' => 'width: 100%;')) }}
            </p>
          </div>
        </div>

        <div class="panel panel-default">
          <div class="panel-heading strong" id="Aktualnosci-label">
            {{ ZkHtml::errorIcon('Attachments') }} ZAŁĄCZNIKI
          </div>
          <div class="panel-body">
            <p>
              ...Tu opiszemy, jak działają załączniki...
            </p>
          </div>
          <table class="table">
            <thead id="attachment-header">
              <tr>
                <th>Nowy?</th>
                <th>Plik</th>
                <th>Wielkość</th>
                <th>Usuń</th>
              </tr>
            </thead>
            <tbody id="attachment-table">
              <!-- The contents of this table is created dymically -->
            </tbody>
          </table>
          <div class="panel-body">
            <button type="button" id="add-attachment" class="btn btn-xs btn-zk">Dodaj załącznik</button>
          </div>
        </div>

        <div class="panel panel-default">
          <div class="panel-heading strong" id="Tresc-label">
            {{ ZkHtml::errorIcon('Tresc') }} TREŚĆ
          </div>
          <div class="panel-body">
            <p>
              {{ Form::textarea( 'Tresc', null, array(
                'style' => 'width: 100%;', 'name' => 'Tresc', 'id' => 'Tresc',
                'data-validate' => 'true', 'data-min-length' => 10, 'data-max-length' => 5000 ) ) }}
            </p>
          </div>
        </div>

        <p>
          {{ Form::submit('Zapisz', array(
            'class' => 'btn btn-xs btn-zk', 'id' => 'btn-submit')) }}
        </p>

{{ Form::close() }}

      </td>
    </tr>
  </table>
</div>

<script>
  $( document ).ready( function() {
    CKEDITOR.replace( 'Tresc' );
    CKEDITOR.instances[ 'Tresc' ].on( 'blur', function() { this.updateElement(); } );

    ZKGallery.init();

    <!--
      TODO: Deal with attachments that were added and then marked for deletion directly client-side
      TODO: Implement the rest of attachments (routes and model).
    -->

    document.ZKGlobal = { 'attachmentRows': 0 };
    $( "#attachment-header" ).hide();
    $( "#add-attachment" ).click( function() {
      var num = ++document.ZKGlobal.attachmentRows;

      var tr = $( '<tr></tr>' );

      $( '<td>Tak</td>' ).appendTo( tr );

      var tdFile = $( '<td></td>' );
      $( '<input id="NowyZalacznik' + num + 'Plik" data-validate="true" data-ascii-only="true" name="NowyZalacznik' + num + 'Plik" type="file"></input>' )
        .appendTo( tdFile );
      tdFile.appendTo( tr )

      $( '<td>?</td>' ).appendTo( tr );

      var tdRemove = $( '<td></td>' );
      $( '<input name="NowyZalacznik' + num + 'Usun" value="Usun" type="checkbox"></input>' ).appendTo( tdRemove );
      tdRemove.appendTo( tr );

      if( num == 1 ) {
        $( "#attachment-header" ).show();
      }

      $( "#attachment-table" ).append( tr );
    } );  // #add-attachment.click

    $( '#btn-submit' ).click( function() {

      // Client-side validation
      if( !ZKValidator.validate() ) {
        return false;
      }

      // Grab the id of the active "Obrazek" tab and copy it to the corresponding hidden field.
      var selectedTab = $( '#obrazki-tab li.active' ).attr( 'id' );
      $( '#obrazek-tab-hidden' ).val( selectedTab );

      // Check to see if the user chose a file to upload and then switched to a different tab.
      if( selectedTab != 'tab-upload-hdr' && $( '#obrazek-upload-field' ).val() != '' ) {
        // Recreate the file upload field - there is no way to reset it.
        $( '#obrazek-upload-field-wrapper' ).html(
          "<input id='obrazek-upload-field' name='ObrazekPlik' type='file'>" );
      }

      return true;
    } );  // #btn-submit.click

  } ); // document.ready
</script>

@stop
