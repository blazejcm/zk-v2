@extends('layouts.main')

@section('main')

<?php
function formatAktualnoscImg( $aktualnosc )
{
  if( $aktualnosc->Obrazek != null )
  {
    $path = Aktualnosc::obrazekPath( $aktualnosc->Obrazek );
    if( file_exists( $path ) )
    {
      $size = getimagesize( $path );
      $width = $size[ 0 ];
      $height = $size[ 1 ];
      $url = asset( 'img/aktualnosci/' . $aktualnosc->Obrazek );
      return "<img hspace='10' vspace='5' border='0' align='left' width='$width' height='$height' src='$url'></img>";
    }
  }
  return "";
}

function openAktualnoscImgLink( $aktualnosc )
{
  if( $aktualnosc->ObrazekUrl != null )
  {
    if( Str::contains( $aktualnosc->ObrazekUrl, "://" ) )
    {
      // This is an external link.
      return "<a href='" . $aktualnosc->ObrazekUrl . "' class='link1' target='_blank'>";
    }
    else if( Str::contains( $aktualnosc->ObrazekUrl, "." ) )
    {
      // This is a link to an uploaded document.
      return "<a href='" . asset( 'pliki/aktualnosci/' . $aktualnosc->ObrazekUrl ) . "' class='link1'>";
    }
    else
    {
      // This is a link to another page on the web site.
      return "<a href='http://www.zeszytykomiksowe.org/" . $aktualnosc->ObrazekUrl . "' class='link1'>";
    }
  }
  return "";
}

function closeAktualnoscImgLink( $aktualnosc )
{
  return $aktualnosc->ObrazekUrl != null ? "</a>" : "";
}
?>

<div id="news">

  <p style="background-color:#f8f0ef; border-style:solid; border-color:#d63f2b; border-width:1px; padding-top:8px; padding-bottom:8px;">
    <b><span class="franctytul">Zeszyty</span> zmieniają formułę</b> -
    będziemy publikowali <b>więcej tekstów niezwiązanych z tematem numeru</b>.
    Osoby zainteresowane publikacją tekstu naukowego, popularno-naukowego lub publicystycznego nt. komiksu
    <a href="mailto:redakcja@zeszytykomiksowe.org">proszone są o kontakt z redakcją</a></span>.
  </p>

<?php

$aktualnosci = Aktualnosc::orderBy( 'id', 'DESC' )->paginate( 30 );

echo $aktualnosci->links();

echo link_to_route('aktualnosc.create', "Nowa aktualność", null, array('class' => 'btn btn-xs btn-zk'));

foreach ($aktualnosci as $aktualnosc):
?>
<h3 class="section">
  {{{ $aktualnosc->Data }}}:
  {{ link_to_route('aktualnosc.edit', "Popraw", array('id' => $aktualnosc->id), array('class' => 'btn btn-xs btn-zk')) }}
</h3>

{{ openAktualnoscImgLink( $aktualnosc ) }}
{{ formatAktualnoscImg( $aktualnosc ) }}
{{ closeAktualnoscImgLink( $aktualnosc ) }}

<p>
  {{ $aktualnosc->Tresc }}
  <br clear="all" />
</p>
<?php
endforeach;
?>

</div>

<div id="side-bar">
  <h3 class="section">dotacja:</h3>
  Magazyn <span class="franctytul">Zeszyty Komiksowe</span> jest współfinansowany przez
  <img src="mkidn.png" align="top" width="175" height="106">.

  <h3 class="section">reklamy:</h3>
  <a href="/aktualnosci/upload/Info%20prasowe_komiks.pdf" target="_blank">
    <img src="/obrazki/katalog_wypadkow_baner.jpg" width="200" height="150" />
  </a>
  <br>&nbsp;<br>
  <a href="http://galeria-bwa.karkonosze.com/akt.htm/index.php?mod=news&type=user&action=view" target="_blank">
    <img src="/obrazki/Trust-bnr.jpg" width="200" height="163" />
  </a>

  <h3 class="section">kontakt:</h3>
  {{ HTML::mailto('blazej@zeszytykomiksowe.org', 'Michał Błażejczyk (naczelny <span class="franctytul">ZK</span>)') }}
  <br /><br />
  <a href="mailto:redakcja@zeszytykomiksowe.org">redakcja <span class="franctytul">ZK</span></a>
  <br /><br />
  <a href="mailto:recenzje@zeszytykomiksowe.org">dział recenzji <span class="franctytul">ZK</span></a>
  <br /><br />
  <a href="mailto:centrala@centrala.org.pl">wydawca i dystrybutor <span class="franctytul">ZK</span></a>
  <br /><br />
  <img src="fb.png" width="16" height="16" />
  <a href="http://www.facebook.com/pages/Zeszyty-Komiksowe/189675144454" target="_blank">facebook</a>

  <h3 class="section">subskrypcja:</h3>
  Chcesz otrzymywać na bieżąco informacje o aktualizacjach tej strony?
  <br /><br />
  <a href="http://zeszytykomiksowe.org/phplist/?p=subscribe" class="active">Zapisz się</a>
  <br />
  <a href="http://zeszytykomiksowe.org/phplist/?p=unsubscribe" class="active">Wypisz się</a>
  <br />
  <a href="http://zeszytykomiksowe.org/phplist/?p=preferences" class="active">Zmień dane</a>
</div>

@stop