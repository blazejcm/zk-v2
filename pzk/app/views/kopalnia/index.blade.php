@extends('layouts.main')

@section('main')

<p class="francrozdzial"><span class="blackcourier">Kopalnia Wiedzy o Komiksie</span></p>

<div align="center">
  <table border="0" width="100%">
    <tbody>
      <tr>
        <td class="blackcourier">

          <p>
            <strong>Kopalnia Wiedzy o Komiksie</strong> (w skrócie KWoK)
            to bibliograficzny katalog (baza danych), zawierający informacje
            o wszelkiego rodzaju publikacjach, dotyczących komiksu: artykułach,
            recenzjach, pracach naukowych, książkach, czasopismach itd., zarówno
            polskojęzycznych, jak i zagranicznych. Zarejestrowani użytkownicy
            mogą <strong>dopisywać komentarze i linki</strong>.
          <p/>

          <p>
            W chwili obecnej Kopalnia zawiera N publikacji.
          </p>

          <p>
            Dodatkowe linki:
            <ul>
              <li>
                {{ link_to_route( 'kopalnia.search', "Wyszukiwanie zaawansowane", null, array( 'class' => 'link1' ) ) }}
              </li>
              <li>
                {{ link_to_route( 'kopalnia.create', "Dodaj nowy wpis do Kopalni", null, array( 'class' => 'link1' ) ) }}
              </li>
            </ul>
          </p>

        </td>
      </tr>
    </tbody>
  </table>
</div>

@stop
