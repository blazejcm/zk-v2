@extends('layouts.main')

@section('main')

    <div class="kopalnia-top-div">
      <div class="col-md-12 kopalnia-content-div">
        <p>
          <a href="#"><span class="recka-title kopalnia-title">{{$kopalnia->Tytul}}</span></a>
          <span class="inobtrusive">({{$kopalnia->PrintRodzaj()}})</span>
        </p>
@if( $kopalnia->RodzicId != null  )
        <p class="blockquote">
          <span class="inobtrusive">w:</span>
          <a href="#">{{$kopalnia->rodzic->Tytul}}</a>
  @if( $kopalnia->DzialId != null )
          <span class="inobtrusive">({{$kopalnia->rodzic->PrintRodzaj()}};
          dział: </span><a href="#">{{$kopalnia->dzial->Tytul}}</a><span class="inobtrusive">)</span>
  @else
          <span class="inobtrusive">({{$kopalnia->rodzic->PrintRodzaj()}})</span>
  @endif
        </p>
@elseif( $kopalnia->Rodzic != null  )
        <p class="blockquote">
          <span class="inobtrusive">w:</span>
          <a href="#">{{$kopalnia->Rodzic}}</a>
        </p>
@endif

@if( $kopalnia->autorzy->count() > 0 )
        <p>
        {{$kopalnia->autorzyView()}}
        </p>
@endif

@if( $kopalnia->wydawcy->count() > 0 || $kopalnia->MiejsceWydania != null )
        <p>
  @if( $kopalnia->wydawcy->count() > 0 )
    {{$kopalnia->wydawcyView().($kopalnia->MiejsceWydania != null ? ", " : "")}}
  @endif
  @if( $kopalnia->MiejsceWydania != null )
    {{$kopalnia->MiejsceWydania.($kopalnia->PubRok != null ? ": ".$kopalnia->PubRok : "")}}
  @endif
        </p>
@endif

@if( $kopalnia->PubMiesiac != null && $kopalnia->PubRok != null )
        <p>
          <span class="inobtrusive">Data publikacji:</span> {{Converter::Miesiac($kopalnia->PubMiesiac)}} {{$kopalnia->PubRok}}
        </p>
@endif

@if( $kopalnia->Isbn != null )
        <p>
          <span class="inobtrusive">ISBN:</span> {{$kopalnia->Isbn}}
        </p>
@endif

@if( $kopalnia->Objetosc != null )
        <p>
          <span class="inobtrusive">Objętość:</span> {{$kopalnia->Objetosc}}
        </p>
@endif

@if( $kopalnia->Jezyk != null )
        <p>
          <span class="inobtrusive">Język:</span> {{$kopalnia->PrintJezyk()}}
        </p>
@endif
      </div>
    </div>
    <div class="row kopalnia-top-div no-margin">
      <div class="col-md-8 kopalnia-content-div">

@if( $kopalnia->haslaPrzedmiotowe->count() > 0 )
        <p class="inobtrusive">
          Hasła przedmiotowe:
        </p>
        <p class="blockquote">
          {{$kopalnia->haslaPrzedmiotoweView()}}
        </p>
@endif

@if( $kopalnia->Opis != null && $kopalnia->Opis != "" )
        <p class="inobtrusive">
          Opis:
        </p>
        <p class="blockquote">
          {{$kopalnia->Opis}}
        </p>
@endif

      </div>

      <div class="col-md-4 kopalnia-content-div left-dashed-border">

@if( $kopalnia->SlowaKluczowe != null && $kopalnia->SlowaKluczowe != "" )
        <p class="inobtrusive">
          Słowa kluczowe:
        </p>
        <p class="blockquote">
          {{$kopalnia->SlowaKluczowe}}
        </p>
@endif

        <p class="inobtrusive">
          Linki:
        </p>
        <p class="blockquote">
          <a href="#">Recenzja w <em>Gazecie Wyborczej</em></a><br>
          <a href="#">Tekst, którego dotyczy ten artykuł</a>
        </p>
      </div>
    </div>

@stop
