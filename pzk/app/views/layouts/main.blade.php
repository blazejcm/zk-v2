<!doctype html>
<html>
  <head>

    <meta charset="utf-8">

    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
    {{ HTML::style( 'css/zk.css' ) }}

    <script>
      ZKPaths = {
        'json': '{{ asset( "json" ) }}',
        'img': '{{ asset( "img" ) }}',
        'root': '{{ asset( "" ) }}'
      };
    </script>

    <script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
    <script src="http://netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
    <script src="{{ asset( 'ckeditor/ckeditor.js' ) }}"></script>
    <!-- {{ HTML::script( 'js/underscore-min.js' ) }} -->
    <script src="{{ asset( 'js/zk-gallery.js' ) }}"></script>
    <script src="{{ asset( 'js/zk-validator.js' ) }}"></script>

  </head>
  <body>

  @if( Session::has( 'problem' ) )
    <div class='alert alert-danger alert-on-top' id='alert'>
      <p>
        {{ Session::get( 'problem' ) }}
      </p>
    </div>
  @endif

  <div id="container">
    <div style="height: 95px;">
      <div id="logo-zk">
        <a href="{{ URL::to( '/' ) }}"><img src="{{ asset('img/ZK_logo_white.png') }}" width="150" height="150" border="0" /></a>
      </div>
      <div id="logo-centrala" style="align: right;">
        <a href="http://centrala.org.pl/"><img src="{{ asset('img/centrala_logo.png') }}" width="110" height="35" border="0" /></a>
        <br>
        <a href="http://lib.amu.edu.pl/"><img src="{{ asset('img/bu_logo.png') }}" width="110" height="38" border="0" /></a>
      </div>
    </div>
    <div id="nav-menu">
      <ul>
        <li>
          {{ link_to( "/", "aktualności" ) }}
        </li>
        <li>
          {{ link_to( "zk", "magazyn" ) }}
        </li>
        <li>
          {{ link_to( "recenzje", "recenzje" ) }}
        </li>
        <li style="width: 150px;">
          &nbsp;
        </li>
        <li>
          {{ link_to( "skladnica", "składnica" ) }}
        </li>
        <li>
          {{ link_to( "kopalnia", "kopalnia" ) }}
        </li>
        <li>
          {{ link_to( "publicystyka", "publicystyka" ) }}
        </li>
      </ul>
    </div>

    @yield('main')


<!-------------------------------- FOOTER -------------------------------->

    <div id="footer">
      <div id="footer-slogan">
        O komiksie. Na serio.
      </div>
      <div id="footer-copy">
        layout &copy; Michał Błażejczyk 2012
        &nbsp;&nbsp;||&nbsp;&nbsp;
        logo &copy; {{ HTML::mailto('dennis.wojda@gmail.com', 'Dennis Wojda') }} 2011
      </div>
      <div id="footer-about">
        {{ link_to( "o_stronie", "O stronie" ) }}
      </div>
    </div>
  </div>
</body>

@if( Session::has( 'problem' ) )
  <script>
    $( document ).ready( function() {
      window.setTimeout( function() { $( "#alert" ).fadeOut( 1500, function() {} ); }, 2000 );
    } );
  </script>
@endif

</html>
