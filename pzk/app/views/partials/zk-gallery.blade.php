<!-- Modal used as the image gallery. -->
<div class="modal fade" id="gallery-modal" tabindex="-1" role="dialog" aria-labelledby="gallery-modal-label" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="gallery-modal-label">Wybierz obrazek</h4>
      </div>
      <div class="modal-body">
        <div id="ajax-loading">
          <img src="{{ asset( 'img/ajax-loader.gif' ) }}" style="display: block; margin-left: auto; margin-right: auto;">
        </div>
        <ul class="pager">
          <li><a id="prev-page" href="#" class="btn disabled">Poprzednie</a></li>
          <li><a id="next-page" href="#" class="btn">Następne</a></li>
        </ul>
        <table width="100%" id="gallery-table" style="display: none">
        </table>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-xs btn-default" data-dismiss="modal">Anuluj</button>
        <button type="button" class="btn btn-xs btn-zk" id="gallery-ok-btn">Wybierz</button>
      </div>
    </div>
  </div>
</div>
