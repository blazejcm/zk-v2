/**
 * Created by Michal on 19/06/14.
 */

CKEDITOR.plugins.add( 'title', {
  icons: 'title',
  init: function( editor ) {
    var style = new CKEDITOR.style( editor.config.coreStyles_title );
    editor.attachStyleStateChange( style, function( state ) {
      !editor.readOnly && editor.getCommand( 'Title' ).setState( state );
    } );

    editor.addCommand( 'Title', new CKEDITOR.styleCommand( style ) );
    editor.ui.addButton( 'Title', {
      label: 'Tytuł',
      command: 'Title',
      icon: CKEDITOR.plugins.getPath( 'title' ) + 'icons/title.png'
    } );
  }
} );

// The basic configuration that you requested
CKEDITOR.config.coreStyles_title = { element: 'span', attributes: { 'class': 'tytul' } };
