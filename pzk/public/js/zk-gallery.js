ZKGallery = {
  // To make this function work, you must include file XXX in your HTML file...
  'init': function() {
    document.zkGallery = { 'loaded': false,
                           'selectedItem': undefined,
                           'selectedUrl': '',
                           'currentPage': 1 };

    $( '#trigger-popup' ).click( function() {

      if( !document.zkGallery.loaded ) {

        $.getJSON( ZKPaths.json + '/obrazki/aktualnosci', function( data ) {

          document.zkGallery.items = data;

          document.zkGallery.maxPage = Math.floor( document.zkGallery.items.length / 24 ) + 1;

          // It is much more efficient to run this in a single loop than to use underscore.js
          var htmlArr = new Array();
          htmlArr.push( '<tr class="gallery-page1">' );
          for( var i = 0; i < document.zkGallery.items.length; ++i ) {
            htmlArr.push( '<td class="gallery-td"><img class="gallery-img" src="'
              + ZKPaths.img + '/aktualnosci/' + document.zkGallery.items[ i ].path
              + '" data-url="' + document.zkGallery.items[ i ].path + '"></td>' );
            if( ( i + 1 ) % 8 == 0 ) {
              var page = Math.floor( ( i + 1 ) / 24 ) + 1;
              var style = ( page > 1 ? ' style="display: none"' : "" );
              htmlArr.push( '</tr><tr class="gallery-page' + page + '"' + style + '>' );
            }
          };
          htmlArr.push( '</tr>' );

          var table = $( "#gallery-table" );
          table.append( htmlArr.join( '' ) );
          table.show();

          // attach the onClick handler to all gallery images
          $( ".gallery-img" ).click( function() {
            if( document.zkGallery.selectedItem !== undefined ) {
              document.zkGallery.selectedItem.removeClass( "gallery-img-sel" );
            }
            document.zkGallery.selectedItem = $( this );
            document.zkGallery.selectedUrl = $( this ).attr( 'data-url' );
            $( this ).addClass( "gallery-img-sel" );
          } );

          $( "#prev-page" ).click( function() {
            if( document.zkGallery.currentPage > 1 ) {
              $( ".gallery-page" + document.zkGallery.currentPage ).hide();
              document.zkGallery.currentPage--;
              $( ".gallery-page" + document.zkGallery.currentPage ).show();

              if( document.zkGallery.currentPage == document.zkGallery.maxPage - 1 ) {
                $( "#prev-page" ).removeClass( "disabled" );
                $( "#next-page" ).removeClass( "disabled" );
              } else if( document.zkGallery.currentPage == 1 ) {
                $( "#prev-page" ).addClass( "disabled" );
              }
            }
            return false;
          } );

          $( "#next-page" ).click( function() {
            if( document.zkGallery.currentPage < document.zkGallery.maxPage ) {
              $( ".gallery-page" + document.zkGallery.currentPage ).hide();
              document.zkGallery.currentPage++;
              $( ".gallery-page" + document.zkGallery.currentPage ).show();
   
              if( document.zkGallery.currentPage == 2 ) {
                $( "#next-page" ).removeClass( "disabled" );
                $( "#prev-page" ).removeClass( "disabled" );
              } else if( document.zkGallery.currentPage == document.zkGallery.maxPage ) {
                $( "#next-page" ).addClass( "disabled" );
              }
            }
            return false;
          } );

          $( "#gallery-ok-btn" ).click( function() {            
            if( document.zkGallery.selectedUrl !== undefined ) {
              $( '#obrazek-field' ).val( document.zkGallery.selectedUrl );
              $( '#gallery-modal' ).modal( 'hide' );
            }
            return false;
          } );

          $( "#ajax-loading" ).hide();

          document.zkGallery.loaded = true;
        } ); // $.getJSON( ... )

      } // if( !document.zkGallery.loaded ) ...

      return true;
    } ); // $( '#trigger-popup' ).click( ... )
  }
};
