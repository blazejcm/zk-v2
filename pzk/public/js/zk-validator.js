// This object provides client-side validation for forms.
// It depends on properly named ('id' attribute) labels, input elements with
// the appropriate 'data-' attributes, and the presence of the alert box
// (see ZkHtml.php: errorIcon(), label() and formErrorAlert()).
ZKValidator = {
  'validate': function() {
    // See if there were any validation errors.
    var anyErrors = false;
    $( ':input[data-validate=true]' ).each( function( index, elem ) {
      anyErrors = anyErrors || ZKValidator.processElement( $( this ) );
    } );

    // Show or hide the alert window.
    if( anyErrors ) {
      $( '#form-alert' ).html( "<p>Formularz zawiera błędy.</p>" );
      $( '#form-alert' ).show();

      // Scroll the page to show the top of the form with the alert box.
      $( 'html,body' ).animate( {
        scrollTop: $( '#page-header' ).offset().top + 'px'
      }, 'fast' );
    } else {
      $( '#form-alert' ).hide();
    }

    return !anyErrors;
  },

  'processElement': function( elem ) {
    var error = this.validateField( elem );
    var errorSpan = this.getErrorSpan( elem );
    var errorLabel = this.getLabel( elem );
    if( error == '' ) {
      errorSpan.hide();
      errorLabel.removeClass( 'error' );
      return false;
    } else {
      errorSpan.attr( 'title', error );
      errorSpan.show();
      errorLabel.addClass( 'error' );
      return true;
    }
  },

  'getErrorSpan': function( elem ) {
    return $( '#' + elem.attr( 'id' ) + '-error' );
  },

  'getLabel': function( elem ) {
    return $( '#' + elem.attr( 'id' ) + '-label' );
  },

  'validateField': function( elem ) {
    var val = elem.val();
    var minLen = parseInt( elem.attr( 'data-min-length' ) );
    if( minLen !== NaN && val.length < minLen ) {
      return "Tekst za krótki.";
    }
    var maxLen = parseInt( elem.attr( 'data-max-length' ) );
    if( maxLen !== NaN && val.length > maxLen ) {
      return "Tekst za długi.";
    }
    if( elem.attr( 'data-ascii-only' ) ) {
      if( val.search( '[^\u0020-\u007e]' ) != -1 ) {
        return "Nazwa pliku nie powinna zawierać liter akcentowanych.";
      }
    }
    return "";
  }
};
